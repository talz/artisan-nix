{
  description = "A very basic flake";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system}; in
      rec {
        defaultPackage = pkgs.appimageTools.wrapType2 {
          name = "artisan";
          src = pkgs.fetchurl {
            url = "https://github.com/artisan-roaster-scope/artisan/releases/download/v2.4.6/artisan-linux-2.4.6.AppImage";
            sha256 = "sha256-3F6oThkxDiNB+W3NfyBn3GfSMxiUtuWZ19iyKMl1pAA=";
          };
        };
      }
    );
}
